#ifndef PROCESSAIN_HPP
#define PROCESSAIN_HPP

#include "imagem.hpp"

using namespace std;

class ProcessaIn : public Imagem {
	private:
		ifstream * abrirImagem;

	public:
		ProcessaIn();
		ProcessaIn(ifstream * abrirImagem);

		ifstream * getAbrirImagem();
		void setAbrirImagem(ifstream * abrirImagem);

		int Abertura(const char * nome);
};

#endif 
