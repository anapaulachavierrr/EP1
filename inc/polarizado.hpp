#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP

#include "filtro.hpp"

using namespace std;

class Polarizado : public Filtro{
	public:
		Polarizado();
		char ** Filtra(char ** pixelsR, char ** pixelsG, char ** pixelsB, int largura, int altura, int *maximo);
};

#endif
