#ifndef PRETOBRANCO_HPP
#define PRETOBRANCO_HPP

#include "filtro.hpp"

using namespace std;

class Pretobranco : public Filtro {
	public:
		Pretobranco();
		char ** Filtra(char ** pixelsR, char ** pixelsG, char ** pixelsB, int largura, int altura, int *maximo);
};
#endif
