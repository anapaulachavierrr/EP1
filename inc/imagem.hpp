#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;

class Imagem {

	public:
		string numeroMagico, comentario;
		int altura, largura, maximo;
		char **pixelsR;
		char **pixelsG;
		char **pixelsB;

		Imagem();
		Imagem(string numeroMagico, string comentario, int altura, int largura, int maximo, char **pixelsR, char **pixelsG,char **pixelsB);

		string getNumeroMagico();
		void setNumeroMagico(string numeroMagico);

		string getComentario();
		void setComentario(string comentario);

		int getAltura();
		void setAltura(int altura);

		int getLargura();
		void setLargura(int largura);

		int getMaximo();
		void setMaximo(int maximo);

		char ** getPixelsR();
		void setPixelsR(char ** pixelsR);

		char ** getPixelsG();
		void setPixelsG(char ** pixelsG);
	
		char ** getPixelsB();
		void setPixelsB(char ** pixelsB);
};

#endif
