#ifndef MEDIA5X5_HPP
#define MEDIA5X5_HPP

#include "filtro.hpp"

using namespace std;

class Media5x5 : public Filtro{
	public:
		Media5x5();
		char ** Filtra(char ** pixelsR, char ** pixelsG, char ** pixelsB, int largura, int altura, int *maximo);
};

#endif
