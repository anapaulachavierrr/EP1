#ifndef MEDIA3X3_HPP
#define MEDIA3X3_HPP

#include "filtro.hpp"

using namespace std;

class Media3x3 : public Filtro{
	public:
		Media3x3();
		char ** Filtra(char ** pixelsR, char ** pixelsG, char ** pixelsB, int largura, int altura, int *maximo);
};

#endif
