#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP

#include "filtro.hpp"

using namespace std;

class Negativo : public Filtro{
	public:
		Negativo();
		char ** Filtra(char ** pixelsR, char ** pixelsG, char ** pixelsB, int largura, int altura, int *maximo);
};

#endif
