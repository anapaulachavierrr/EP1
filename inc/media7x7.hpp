#ifndef MEDIA7X7_HPP
#define MEDIA7X7_HPP

#include "filtro.hpp"

using namespace std;

class Media7x7 : public Filtro{
	public:
		Media7x7();
		char ** Filtra(char ** pixelsR, char ** pixelsG, char ** pixelsB, int largura, int altura, int *maximo);
};

#endif
