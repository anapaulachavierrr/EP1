#include <iostream>

#include "processaIn.hpp"
#include "processaOut.hpp"
#include "negativo.hpp"
#include "pretobranco.hpp"
#include "polarizado.hpp"
#include "media3x3.hpp"
#include "media5x5.hpp"
#include "media7x7.hpp"


using namespace std;

int main() {
	
	int max;
	const char * nomeImagem;
	const char * nomeSaida;
	string arquivo;
	string arquivoFinal;
	string opcao;

	char **NovoPixelR;
	char **NovoPixelG;
	char **NovoPixelB;
	
	ifstream imagem;
	ofstream novaImagem;
	
	ProcessaIn processaIn;
	ProcessaOut processaOut;

	Negativo negativo;
	Pretobranco pretobranco;
	Polarizado polarizado;
	Media3x3 media3x3;
	Media5x5 media5x5;
	Media7x7 media7x7;

	cout << "Digite o nome e o caminho da imagem a ser modificada." << endl;
	cin >> arquivo;

	nomeImagem = arquivo.c_str();

	processaIn.setAbrirImagem(&imagem);
	processaOut.setCriarImagem(&novaImagem);

	if(processaIn.Abertura(nomeImagem)) {
		cout << "Não foi possível abrir o arquivo!" << endl;
		return 0;
	}
	cout << "\nA imagem está em: " << arquivo << endl;

	cout << "\nDigite o caminho e o nome da novaimagem(ex: ../doc/image.ppm)." << endl;
	cin >> arquivoFinal;

	
	while(1){
		cout << endl << "Escolha um filtro:\n";
		cout << "Negativo: 1\n" << "Preto e branco: 2\n" << "Polarizado: 3\n" << "Media 3x3: 4\n" << "Media 5x5: 5\n" << "Media 7x7: 6\n" << "Opção: ";
		cin >> opcao;
		if(opcao == "1" || opcao == "2" || opcao == "3"|| opcao == "4"|| opcao == "5"|| opcao == "6")
			break;
		cout << "Número inválido!\n";
	}
		
	max = processaIn.getMaximo();

	if(opcao == "1"){
		NovoPixelR = negativo.Filtra(processaIn.getPixelsR(),processaIn.getPixelsG(),processaIn.getPixelsB(), processaIn.getLargura(), processaIn.getAltura(), &max);

	}

	else if(opcao == "2"){
		NovoPixelR = pretobranco.Filtra(processaIn.getPixelsR(),processaIn.getPixelsG(),processaIn.getPixelsB(), processaIn.getLargura(), processaIn.getAltura(), &max);
	
	}
	
	else if(opcao == "3"){
		NovoPixelR = polarizado.Filtra(processaIn.getPixelsR(),processaIn.getPixelsG(),processaIn.getPixelsB(), processaIn.getLargura(), processaIn.getAltura(), &max);

	}
	else if(opcao == "4"){
		NovoPixelR = media3x3.Filtra(processaIn.getPixelsR(),processaIn.getPixelsG(),processaIn.getPixelsB(), processaIn.getLargura(), processaIn.getAltura(), &max);

	}
	else if(opcao == "5"){
		NovoPixelR = media5x5.Filtra(processaIn.getPixelsR(),processaIn.getPixelsG(),processaIn.getPixelsB(), processaIn.getLargura(), processaIn.getAltura(), &max);
	}
	else if(opcao == "6"){
		NovoPixelR = media7x7.Filtra(processaIn.getPixelsR(),processaIn.getPixelsG(),processaIn.getPixelsB(), processaIn.getLargura(), processaIn.getAltura(), &max);
	}

	
	nomeSaida = arquivoFinal.c_str();
	
	processaOut.setNumeroMagico(processaIn.getNumeroMagico());
	processaOut.setComentario(processaIn.getComentario());
	processaOut.setLargura(processaIn.getLargura());
	processaOut.setAltura(processaIn.getAltura());
	processaOut.setMaximo(max);
	processaOut.setPixelsR(NovoPixelR);

	
	if(processaOut.Criacao(nomeSaida)){
		cout << "Não foi possível criar uma nova imagem!" << endl;
		return 0;
	}
	
	cout << "Nova imagem criada com sucesso!" << endl;
	
	return 0;

}
