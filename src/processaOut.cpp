#include "processaOut.hpp"

using namespace std;

ProcessaOut::ProcessaOut() {
	setCriarImagem(NULL);
}
//Criar nova imagem:
ProcessaOut::ProcessaOut(ofstream * criarImagem) {
	setCriarImagem(criarImagem);
}

ofstream * ProcessaOut::getCriarImagem() {
	return criarImagem;
}
void ProcessaOut::setCriarImagem(ofstream * criarImagem) {
	this -> criarImagem = criarImagem;
}

//Método usado para a criação da nova imagem:
int ProcessaOut::Criacao(const char * nome) {
	criarImagem->open(nome);

	if(!(criarImagem->is_open()))
		return -1;

	char ** pixelsR = getPixelsR();
	char ** pixelsG = getPixelsG();
	char ** pixelsB = getPixelsB();

	int **aux_pixelsR;
	int **aux_pixelsG;
	int **aux_pixelsB;

	int largura = getLargura();
	int altura = getAltura();

	*criarImagem << getNumeroMagico() << endl;
	if(getComentario() != "vazio")
		*criarImagem << getComentario() << endl;
	*criarImagem << largura << ' ' << altura << endl << getMaximo() << endl;

	aux_pixelsR = new int * [altura];
	aux_pixelsG = new int * [altura];
	aux_pixelsB = new int * [altura];

	for(int i = 0; i < altura; i++)
		aux_pixelsR[i] = new int[largura*3];

	for(int i = 0; i < altura; i++)
		aux_pixelsG[i] = new int[largura];

	for(int i = 0; i < altura; i++)
		aux_pixelsB[i] = new int[largura];


	
	for(int i=0; i<(altura); i++){
		for(int j=0; j<(largura); j++){

			aux_pixelsR[i][j] = (unsigned int)(pixelsR[i][j]);
			
		}
	}

	for(int i=0; i<(altura); i++){
		for(int j=largura; j<(largura*2); j++){

			aux_pixelsG[i][j-largura] = (unsigned int)(pixelsR[i][j]);
			
		}
	}

	for(int i=0; i<(altura); i++){
		for(int j=largura*2; j<(largura*3); j++){

			aux_pixelsB[i][j-(2*largura)] = (unsigned int)(pixelsR[i][j]);
			
		}
	}
	for(int i = 0; i < (altura); i++){
		for(int j = 0; j < (largura); j++){
			
			criarImagem->put((char)aux_pixelsR[i][j]);
			criarImagem->put((char)aux_pixelsG[i][j]);
			criarImagem->put((char)aux_pixelsB[i][j]);
		}
	}
	
	
	criarImagem->close();
	
	return 0;

}
