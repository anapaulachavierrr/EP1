#include "media5x5.hpp"
#include <stdio.h>
#include <sstream>

using namespace std;

#include <math.h>

Media5x5::Media5x5(){
}

char ** Media5x5::Filtra(char ** pixelsR, char ** pixelsG, char ** pixelsB, int largura, int altura, int *maximo){
	
	char ** NovoPixelR;
	char ** NovoPixelG;
	char ** NovoPixelB;
	double** filter;
 

	//declaração das matrizes de saída:
	NovoPixelR = new char*[altura];
	for(int i = 0; i < altura; i++)
		NovoPixelR[i] = new char[largura*3];
	
	NovoPixelG = new char*[altura];
	for(int i = 0; i < altura; i++)
		NovoPixelG[i] = new char[largura];

	NovoPixelB = new char*[altura];
	for(int i = 0; i < altura; i++)
		NovoPixelB[i] = new char[largura];

	int valueR=0, valueG=0,valueB=0;


	for(int i=2; i<(altura-2); i++){
		for(int j=2; j<(largura-2); j++){

			//	valueR =0;
			//	valueG =0;
			//	valueB =0;
			for(int x=-2; x<=2; x++){
				for(int y=-2; y<=2; y++){

					valueR += pixelsR[i+x][j+y];
					valueG += pixelsG[i+x][j+y];
					valueB += pixelsB[i+x][j+y]; 

				}
			}

			valueR /=(25);
			valueG /=(25);
			valueB /=(25);

			NovoPixelR[i][j] = valueR;
			NovoPixelG[i][j] = valueG;
			NovoPixelB[i][j] = valueB;

			//printf("%f\n",mediaR);
		}
	}



	for(int i=0; i<(altura); i++){
		for(int j=0; j<(largura); j++){

			NovoPixelR[i][j] = NovoPixelR[i][j];
			
		}
	}

	for(int i=0; i<(altura); i++){
		for(int j=largura; j<(largura*2); j++){

			NovoPixelR[i][j] = NovoPixelG[i][j-largura];
			
		}
	}

	for(int i=0; i<(altura); i++){
		for(int j=largura*2; j<(largura*3); j++){

			NovoPixelR[i][j] = NovoPixelB[i][j-(2*largura)];
			
		}
	}


	return NovoPixelR;
}
