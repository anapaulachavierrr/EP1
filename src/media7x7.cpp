#include "media7x7.hpp"
#include <stdio.h>
#include <sstream>

using namespace std;

#include <math.h>

Media7x7::Media7x7(){
}

char ** Media7x7::Filtra(char ** pixelsR, char ** pixelsG, char ** pixelsB, int largura, int altura, int *maximo){
	
	char ** NovoPixelR;
	char ** NovoPixelG;
	char ** NovoPixelB;
	double** filter;
 

	//declaração das matrizes de saída:
	NovoPixelR = new char*[altura];
	for(int i = 0; i < altura; i++)
		NovoPixelR[i] = new char[largura*3];
	
	NovoPixelG = new char*[altura];
	for(int i = 0; i < altura; i++)
		NovoPixelG[i] = new char[largura];

	NovoPixelB = new char*[altura];
	for(int i = 0; i < altura; i++)
		NovoPixelB[i] = new char[largura];

	int valueR=0, valueG=0,valueB=0;


	for(int i=3; i<(altura-3); i++){
		for(int j=3; j<(largura-3); j++){

			//	valueR =0;
			//	valueG =0;
			//	valueB =0;
			for(int x=-3; x<=3; x++){
				for(int y=-3; y<=3; y++){

					valueR += pixelsR[i+x][j+y];
					valueG += pixelsG[i+x][j+y];
					valueB += pixelsB[i+x][j+y]; 

				}
			}

			valueR /=(49);
			valueG /=(49);
			valueB /=(49);

			NovoPixelR[i][j] = valueR;
			NovoPixelG[i][j] = valueG;
			NovoPixelB[i][j] = valueB;

			//printf("%f\n",mediaR);
		}
	}



	for(int i=0; i<(altura); i++){
		for(int j=0; j<(largura); j++){

			NovoPixelR[i][j] = NovoPixelR[i][j];
			
		}
	}

	for(int i=0; i<(altura); i++){
		for(int j=largura; j<(largura*2); j++){

			NovoPixelR[i][j] = NovoPixelG[i][j-largura];
			
		}
	}

	for(int i=0; i<(altura); i++){
		for(int j=largura*2; j<(largura*3); j++){

			NovoPixelR[i][j] = NovoPixelB[i][j-(2*largura)];
			
		}
	}


	return NovoPixelR;
}
