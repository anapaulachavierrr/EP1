#include "media3x3.hpp"
#include <stdio.h>
#include <sstream>

using namespace std;

#include <math.h>

Media3x3::Media3x3(){
}

char ** Media3x3::Filtra(char ** pixelsR, char ** pixelsG, char ** pixelsB, int largura, int altura, int *maximo){
	char ** NovoPixelR;
	char ** NovoPixelG;
	char ** NovoPixelB;
	double** filter;
 

	//declaração das matrizes de saída:
	NovoPixelR = new char*[altura];
	for(int i = 0; i < altura; i++)
		NovoPixelR[i] = new char[largura*3];
	
	NovoPixelG = new char*[altura];
	for(int i = 0; i < altura; i++)
		NovoPixelG[i] = new char[largura];

	NovoPixelB = new char*[altura];
	for(int i = 0; i < altura; i++)
		NovoPixelB[i] = new char[largura];

	int valueR=0, valueG=0,valueB=0;

	filter = new double*[3];
	for(int i = 0; i <3; i++)
		filter[i] = new double[3];


	int somaR=0.0, somaG=0.0,somaB=0.0;


	for(int i=1; i<(altura-1); i++){
		for(int j=1; j<(largura-1); j++){

			valueR =0;
			valueG =0;
			valueB =0;

	//		NovoPixelR[0][j] = pixelsR[0][j];
	//		NovoPixelG[0][j] = pixelsG[0][j];
	//		NovoPixelB[0][j] = pixelsB[0][j];

			for(int x=-1; x<=1; x++){
				for(int y=-1; y<=1; y++){

					valueR += pixelsR[i+x][j+y];
					valueG += pixelsG[i+x][j+y];
					valueB += pixelsB[i+x][j+y]; 

				}
			}

			valueR /=(9);
			valueG /=(9);
			valueB /=(9);

			NovoPixelR[i][j] = valueR;
			NovoPixelG[i][j] = valueG;
			NovoPixelB[i][j] = valueB;

			//printf("%f\n",mediaR);
		}
	}



	for(int i=0; i<(altura); i++){
		for(int j=1; j<(largura); j++){

			NovoPixelR[i][j] = NovoPixelR[i][j];
			
		}
	}

	for(int i=0; i<(altura); i++){
		for(int j=largura; j<(largura*2); j++){

			NovoPixelR[i][j] = NovoPixelG[i][j-largura];
			
		}
	}

	for(int i=0; i<(altura); i++){
		for(int j=largura*2; j<(largura*3); j++){

			NovoPixelR[i][j] = NovoPixelB[i][j-(2*largura)];
			
		}
	}


	return NovoPixelR;
}
