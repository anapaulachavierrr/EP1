#include "processaIn.hpp"

using namespace std;

ProcessaIn::ProcessaIn() {
	setAbrirImagem(NULL);
}

//Abrir imagem:
ProcessaIn::ProcessaIn(ifstream * abrirImagem) {
	setAbrirImagem(abrirImagem);
}

ifstream * ProcessaIn::getAbrirImagem() {
	return abrirImagem;
}
void ProcessaIn::setAbrirImagem(ifstream * abrirImagem) {
	this -> abrirImagem = abrirImagem;
}

//Método usado:
int ProcessaIn::Abertura(const char * nome) {

	abrirImagem->open(nome);
	if(!(abrirImagem->is_open()))
		return -1;
	
	int altura, largura, maximo;
	string numeroMagico, comentario;
	stringstream a;
	
	getline(*abrirImagem, numeroMagico);
	if(numeroMagico != "P6")
		return -1;
	
	setNumeroMagico(numeroMagico);
	getline(*abrirImagem, comentario);
	if(comentario[0] == '#'){
		setComentario(comentario);
		*abrirImagem >> largura >> altura;
	}
	else{
		a.str(comentario);
		a >> largura >> altura;
	}
	*abrirImagem >> maximo;
	
	setLargura(largura);
	setAltura(altura);
	setMaximo(maximo);
	
	abrirImagem->get();

	
	pixelsR = new char*[altura];
	for(int i = 0; i < altura; i++)
	pixelsR[i] = new char[largura];

	pixelsG = new char*[altura];
	for(int i = 0; i < altura; i++)
	pixelsG[i] = new char[largura];

	pixelsB = new char*[altura];
	for(int i = 0; i < altura; i++)
	pixelsB[i] = new char[largura];

	if((pixelsR == NULL) || (pixelsG == NULL) || (pixelsB == NULL)){
		return -1;
	}

	setPixelsR(pixelsR);
	setPixelsG(pixelsG);
	setPixelsB(pixelsB);
		
	for(int i = 0; i < (altura); i++){
		for(int j = 0; j < (largura); j++){

			abrirImagem->get(pixelsR[i][j]);
			abrirImagem->get(pixelsG[i][j]);
			abrirImagem->get(pixelsB[i][j]);
		}
	}
	
	
	abrirImagem->close();
	
	return 0;
}
