#include "imagem.hpp"

Imagem::Imagem(){
	setNumeroMagico("P5");
	setComentario("ConstrutorVazio");
	setAltura(255);
	setLargura(255);
	setMaximo(255);
	setPixelsR(NULL);
	setPixelsG(NULL);
	setPixelsB(NULL);
}

Imagem::Imagem(string numeroMagico, string comentario, int altura, int largura, int maximo, char **pixelsR, char **pixelsG,char **pixelsB) {
	setNumeroMagico(numeroMagico);
	setComentario(comentario);
	setAltura(altura);
	setLargura(largura);
	setMaximo(maximo);
	setPixelsR(pixelsR);
	setPixelsG(pixelsG);
	setPixelsB(pixelsB);
}

string Imagem::getNumeroMagico() {
	return numeroMagico;
}
void Imagem::setNumeroMagico(string numeroMagico) {
	this -> numeroMagico = numeroMagico;
}

string Imagem::getComentario() {
	return comentario;
}
void Imagem::setComentario(string comentario) {
	this -> comentario = comentario;
}

int Imagem::getAltura() {
	return altura;
}
void Imagem::setAltura(int altura) {
	this -> altura = altura;
}

int Imagem::getLargura() {
	return largura;
}
void Imagem::setLargura( int largura) {
	this -> largura = largura;
}

int Imagem::getMaximo() {
	return maximo;
}
void Imagem::setMaximo(int maximo) {
	this -> maximo = maximo;
}

char ** Imagem::getPixelsR() {
	return pixelsR;
}
void Imagem::setPixelsR(char ** pixelsR) {
	this -> pixelsR = pixelsR;
}

char ** Imagem::getPixelsG() {
	return pixelsG;
}
void Imagem::setPixelsG(char ** pixelsG) {
	this -> pixelsG = pixelsG;
}

char ** Imagem::getPixelsB() {
	return pixelsB;
}
void Imagem::setPixelsB(char ** pixelsB) {
	this -> pixelsB = pixelsB;
}
