#include "pretobranco.hpp"

using namespace std;

#include <math.h>

Pretobranco::Pretobranco(){
}

char ** Pretobranco::Filtra(char ** pixelsR, char ** pixelsG, char ** pixelsB, int largura, int altura, int *maximo){
	char ** NovoPixelR;
	char ** NovoPixelG;
	char ** NovoPixelB;
	
	//declaração das matrizes de saída:
	NovoPixelR = new char*[altura];
	for(int i = 0; i < altura; i++)
		NovoPixelR[i] = new char[largura];
	
	NovoPixelG = new char*[altura];
	for(int i = 0; i < altura; i++)
		NovoPixelG[i] = new char[largura];

	NovoPixelB = new char*[altura];
	for(int i = 0; i < altura; i++)
		NovoPixelB[i] = new char[largura];
	

	double grayscale_value = 0.0;

	for(int i = 0; i < altura; i++)
		NovoPixelB[i] = new char[largura];
	

	for(int i=0; i<(altura); i++){
		for(int j=0; j<(largura); j++){

			 grayscale_value = (0.299 * pixelsR[i][j]) + (0.587 * pixelsG[i][j]) + (0.144 * pixelsB[i][j]);
		
		NovoPixelR[i][j] = grayscale_value;
		NovoPixelG[i][j] = grayscale_value;
		NovoPixelB[i][j] = grayscale_value;
		
		}
	}
	
	return NovoPixelR;
	return NovoPixelG;
	return NovoPixelB;
}

